package TestingUtils;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ResultLog {

	private static ArrayList<String> testCases;
	private static ArrayList<Boolean> results;
	
	/**
	 * Initialization  
	 */
	public ResultLog(){
		testCases = new ArrayList<String>();
		results = new ArrayList<Boolean>();
	}
	
	/**
	 * 
	 * @param testCaseName
	 * @param result pass or fail
	 */
	public static void addResult(String testCaseName, Boolean result){
		testCases.add(testCaseName);
		results.add(result);
	}
	
	/**
	 * Prints test results to terminal
	 * @return whether or not it printed
	 */
	public static Boolean printResults(){
		if(testCases.size()==0){
			System.out.println("Result Log is empty");
			return false;
		}
		System.out.println();
		System.out.println("-------------TEST RESULTS---------------");
		System.out.println();
		String result;
		for(int i = 0; i < testCases.size(); i++){			
			if(results.get(i)){
				result = "PASS";
			}else{
				result = "FAIL";
			}
			System.out.println(result+": "+testCases.get(i));
		}
		return true;
	}
	/**
	 * Creates and writes test results to a file then opens the file with default application
	 * @return boolean
	 */
	public static Boolean writeResultsToFile(){		
		if(testCases.size()==0){
			System.out.println("Result Log is empty");
			return false;
		}
		try {
			DateFormat dateFormat = new SimpleDateFormat("_dd-MM-yy");
			Date date = new Date();
			String filename = "result_log_"+dateFormat.format(date)+".txt";
			PrintWriter writer = new PrintWriter(filename, "UTF-8");
			writer.println("");
			writer.println("-------------TEST RESULTS---------------");
			writer.println("");
			String result;
			for(int i = 0; i < testCases.size(); i++){			
				if(results.get(i)){
					result = "PASS";
				}else{
					result = "FAIL";
				}
				writer.println(result+": "+testCases.get(i));
			}
			writer.close();
			File file = new File(filename);
			Desktop.getDesktop().open(file );		
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
}
