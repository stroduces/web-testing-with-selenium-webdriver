package TestingUtils;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FireFoxDriver {

	/** The web driver. */
	private final WebDriver webDriver;

	/**
	 * Instantiates a new fire fox driver.
	 */
	public FireFoxDriver() {
		this.webDriver = new FirefoxDriver();
		webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		webDriver.manage().window().maximize();
	}

	/**
	 * Gets the web driver.
	 *
	 * @return the web driver
	 */
	public WebDriver getWebDriver() {
		return webDriver;
	}
}

