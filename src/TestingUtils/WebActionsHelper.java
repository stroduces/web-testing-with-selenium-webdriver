package TestingUtils;

import static org.junit.Assert.fail;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

/**
 * The Class WebActionsHelper.
 */
public final class WebActionsHelper {

	/** The Constant RESET_TO_SECONDS. */
	private static final int RESET_TO_SECONDS = 3;

	/** The Constant TIME_TO_SPEND_LOOKING_MILI. */
	private static final int TIME_TO_SPEND_LOOKING_MILI = 30;

	/** The Constant THREAD_SLEEP_TIME. */
	private static final int THREAD_SLEEP_TIME = 200;

	/**
	 * Instantiates a new web actions helper.
	 */
	private WebActionsHelper() {
	}

	/**
	 * Gets the error label by text.
	 *
	 * @param text the text
	 * @param driver the driver
	 * @return the error label by text
	 * @throws InterruptedException the interrupted exception
	 */
	public static WebElement getErrorLabelByText(String text, WebDriver driver) throws InterruptedException {
		Thread.sleep(THREAD_SLEEP_TIME);
		List<WebElement> labels = null;
		//Set time outs looking for elements to zero. 
		driver.manage().timeouts().implicitlyWait(TIME_TO_SPEND_LOOKING_MILI, TimeUnit.MILLISECONDS);
		labels = driver.findElements(By.className("error"));
		//reset to three seconds after searched for element
		driver.manage().timeouts().implicitlyWait(RESET_TO_SECONDS, TimeUnit.SECONDS);

		for (WebElement label : labels) {
			if (text.equals(label.getText())) {
				return label;
			}
		}
		return null;
	}

	/**
	 * Gets an element by id.
	 *
	 * @param id the id
	 * @param driver the driver
	 * @return the element by id
	 */
	public static WebElement getElementById(String id, WebDriver driver) {
		WebElement element = null;
		try {
			//Set time outs looking for elements to zero. 
			driver.manage().timeouts().implicitlyWait(TIME_TO_SPEND_LOOKING_MILI, TimeUnit.MILLISECONDS);
			element = driver.findElement(By.id(id));
			//reset to three seconds after searched for element
			driver.manage().timeouts().implicitlyWait(RESET_TO_SECONDS, TimeUnit.SECONDS);
		} catch (NoSuchElementException e) {
			System.out.println(e.getMessage());
			return null;
		}
		return element;
	}

	/**
	 * Gets an element by xpath.
	 *
	 * @param path
	 * @param driver the driver
	 * @return the element by id
	 */
	public static WebElement getElementByXpath(String xpath, WebDriver driver) {
		WebElement element = null;
		try {
			//Set time outs looking for elements to zero. 
			driver.manage().timeouts().implicitlyWait(TIME_TO_SPEND_LOOKING_MILI, TimeUnit.MILLISECONDS);
			element = driver.findElement(By.xpath(xpath));
			//reset to three seconds after searched for element
			driver.manage().timeouts().implicitlyWait(RESET_TO_SECONDS, TimeUnit.SECONDS);
		} catch (NoSuchElementException e) {
			System.out.println(e.getMessage());
			return null;
		}
		return element;
	}
	
	/**
	 * Gets an element by class name.
	 *
	 * @param name the classname
	 * @param driver the driver
	 * @return the element by id
	 */
	public static WebElement getElementByClassname(String name, WebDriver driver) {
		WebElement element = null;
		try {
			//Set time outs looking for elements to zero. 
			driver.manage().timeouts().implicitlyWait(TIME_TO_SPEND_LOOKING_MILI, TimeUnit.MILLISECONDS);
			element = driver.findElement(By.className(name));
			//reset to three seconds after searched for element
			driver.manage().timeouts().implicitlyWait(RESET_TO_SECONDS, TimeUnit.SECONDS);
		} catch (NoSuchElementException e) {
			System.out.println(e.getMessage());
			return null;
		}
		return element;
	}

	
	/**
	 * Gets the element by text.
	 *
	 * @param text the text
	 * @param driver the driver
	 * @return the element by text
	 */
	public static WebElement getElementByText(String text, WebDriver driver) {
		WebElement element = null;
		try {
			//Set time outs looking for elements to zero. 
			driver.manage().timeouts().implicitlyWait(TIME_TO_SPEND_LOOKING_MILI, TimeUnit.MILLISECONDS);
			element = driver.findElement(By.linkText(text));
			//reset to three seconds after searched for element
			driver.manage().timeouts().implicitlyWait(RESET_TO_SECONDS, TimeUnit.SECONDS);
		} catch (NoSuchElementException e) {
			System.out.println(e.getMessage());
			return null;
		}
		return element;
	}

	/**
	 * *.
	 *
	 * @param driver the driver
	 */
	public static void scrollDown(WebDriver driver) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0,Math.max(document.documentElement.scrollHeight,"
				+ "document.body.scrollHeight,document.documentElement.clientHeight));");
	}

	/**
	 * Gets the inner text by id.
	 *
	 * @param id the id
	 * @param driver the driver
	 * @return the inner textby id
	 */
	public static String getInnerTextbyId(String id, WebDriver driver) {
		String script = "return document.getElementById('" + id + "').innerHTML;";
		return (String) ((JavascriptExecutor) driver).executeScript(script);
	}

	/**
	 * Gets the tag Value by id.
	 *
	 * @param id the id
	 * @param driver the driver
	 * @return the tag Value by id
	 */
	public static String getValuebyId(String id, WebDriver driver) {
		String script = "return document.getElementById('" + id + "').value;";
		return (String) ((JavascriptExecutor) driver).executeScript(script);
	}

		
	/**
	 * Gets the value by id.
	 *
	 * @param id the id
	 * @param driver the driver
	 * @return the value by id
	 */
	public static String getValueById(String id, WebDriver driver) {
		String script = "return document.getElementById('" + id + "').value;";
		return (String) ((JavascriptExecutor) driver).executeScript(script);
	}

	/**
	 * Select by text.
	 *
	 * @param element the element
	 * @param text the text
	 * @param driver the driver
	 */
	public static void selectByText(WebElement element, String text, WebDriver driver) {
		try {
			new Select(element).selectByVisibleText(text);
		} catch (Exception e) {
			//TestResultActions.failure(driver, state, "Couldn't select " + text);
		}
	}
	
	
	public static boolean doesPageContainText(String text, WebDriver driver){
		if(driver.getPageSource().contains(text))
		{
			return true;
		}

		else
		{
		    return false;
		}
	}
	
	/**
	 * Dismisses an alert box
	 * 
	 */
	public static void dismissAlert(WebDriver driver){
		
		Alert alert = null;
		try {
			alert = driver.switchTo().alert();

			if (alert != null) {
				alert.dismiss();
			} else {
				fail("No Alert Generated");
			}
		} catch (UnhandledAlertException e) {
				driver.switchTo().alert().dismiss();
				System.out.println("Alert");
		}
	}
	
	/**
	 * 
	 * @param driver the webdriver
	 * @param element the WebElement on which the click right click will be performed
	 * @param itemIndex the order number in the context menu list you want the driver to click
	 */
	public static void contextMenuOption(WebDriver driver, WebElement element, int itemIndex){
		Actions action= new Actions(driver);
		if(element==null){
			element = getElementById("main", driver);
		}
		action.contextClick(element).perform();
		for(int i = 0; i < itemIndex; i++){
			action.sendKeys(Keys.ARROW_DOWN).perform();
			waitForSeconds(200);
		}
		waitForSeconds(500);
		action.sendKeys(Keys.RETURN).perform();
	}
	
	/**
	 * This is a pretty lazy way for the browser to catch up with the test cases, 
	 * Ideally use Webdriver's built in catchup timer but for tests it might be needed.
	 * 
	 * @param wait time
	 */
	public static void waitForSeconds(long lg){
		try {
			Thread.sleep(lg);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @param driver
	 * @return true or false depending on whether the driver has been manually closed.
	 */
	public static boolean hasQuit(WebDriver driver) {
	    return (driver.toString().contains("(null)")) ? true : false;
	}
	
}
