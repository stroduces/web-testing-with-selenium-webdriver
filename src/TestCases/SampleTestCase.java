package TestCases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import TestingUtils.ResultLog;
import TestingUtils.WebActionsHelper;

public class SampleTestCase {
	//URL to start webdriver at
	private String BASE_URL = "http://www.google.ie";
	
	private WebDriver driver;
	
	public SampleTestCase(WebDriver webdriver){
		driver = webdriver;

	}
	
	/**
	 * Executes a pre-defined assortment of test case methods in succession
	 */
	public void executeTestPlan(){		
		testCaseOpenBaseURL();
	}
	
	private void testCaseOpenBaseURL(){
		driver.get(BASE_URL);
		WebActionsHelper.getElementById("lst-ib", driver).sendKeys("Hello!");
		
	}
	
}
