package Default;

import org.openqa.selenium.WebDriver;

import TestCases.*;
import TestingUtils.FireFoxDriver;
import TestingUtils.ResultLog;

public class Main {

	public static void main(String[] args) {
		
		WebDriver driver;
		FireFoxDriver firefoxDriver = new FireFoxDriver();
		driver = firefoxDriver.getWebDriver();
		
		ResultLog log = new ResultLog();
				
		SampleTestCase siteTests = new SampleTestCase(driver);
		siteTests.executeTestPlan();
		
		ResultLog.printResults();
		ResultLog.writeResultsToFile();
	}

}
