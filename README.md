# README #

### What is this repository for? ###

It's a starting point for an automated testing suite for regression testing websites

### Who is this repository for? ###

Anyone you wants a starting point for their testing suite and doesn't know where to begin. (I would suggest reading up on Selenium Webdriver tutorials if you're unfamiliar)

### How do I get set up? ###

Requires Selenium webdriver jars to be included in your build path: http://www.seleniumhq.org/download/
